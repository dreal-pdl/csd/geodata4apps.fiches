# Migration d'un projet depuis gitlab.com vers Gitlab-forge {#imports_gitlab.com}

## Procédure pas à pas

### Exporter votre projet depuis gitlab.com

Accéder aux paramètres de votre projet sur gitlab.com   
Choisir les menus 'general'/'Advanced' (a déplier)  
![](www/02_a_menu_parametre.png)
Appuyer sur le bouton 'Export project'  
Un message apparaît   
![](www/02_b_message_export.png)
un lien va vous être envoyé par mail, par exemple pour le projet `dreal-datalab/shiny.dsfr` :  
https://gitlab.com/dreal-datalab/shiny.dsfr/download_export  
Vous récupérez grâce à ce lien un dossier zippé.   

### Importer votre projet sur gitlab-forge    

Aller sur [gitlab-forge](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd)  au niveau de votre groupe.

Cliquer sur 'Nouveau projet' / 'import project'

![](www/02_c_nouveau_projet.png)  
![](www/02_d_nouveau_projet.png)  
Le menu `Import project from` s'ouvre, choisir `gitlab export`, puis :   

- saisir le nom du projet,   
- sélectionner le projet zippé exporté depuis gitlab.com.    


`r emo::ji('tada')`  

Si la taille de votre répertoire ou de certains de ses composants excède la taille autorisée sur gitlab-forge, vous pouvez rencontrer une erreur bloquante.  
Recommandation : installez [git filter-repo](https://github.com/newren/git-filter-repo) et/ou suivre le [pas à pas proposé dans l'aide de Gitlab](https://docs.gitlab.com/ee/user/project/settings/import_export.html#import-workarounds-for-large-repositories)
Voici un exemple de script qui nettoie d'un export gitlab avant d'être réimporté créé grâce à ce pas à pas : [maigrir_repo_git.sh] (https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/geodata4apps.fiches/-/raw/main/maigrir_repo_git.sh).

## Adaptations à opérer


### Script d'installation de packages
Dans la documentation du projet, le script R pour installer un package R depuis Gitlab-forge, alors qu'il était auparavant sur l'instance gitlab.com passe de :     
```
remotes::install_gitlab(repo = "adresse_du_groupe/nom_du_package")
```
à :    
```
remotes::install_gitlab(repo = "adresse_du_groupe/nom_du_package", host = "gitlab-forge.din.developpement-durable.gouv.fr")
```

Par exemple : 
```
remotes::install_gitlab(repo = "dreal-pdl/csd/zonages.habitat.r", host = "gitlab-forge.din.developpement-durable.gouv.fr")
```

### Gitlab pages 
Le liens vers les pages html rendue par l'intégration continue de gitlab passent de :  

`https://adresse_du_groupe.gitlab.io/nom_du_projet`

à 

`https://nom_groupe_racine.gitlab-pages.din.developpement-durable.gouv.fr/sous_groupe/projets` 

par exemple : 

`https://dreal-pdl.gitlab-pages.din.developpement-durable.gouv.fr/csd/pages.projets.pub`

### Projet R : déclaration du changement de l'url du dépot distant
Sur votre poste de travail, il reste à faire correspondre votre répertoire local au répertoire distant de gitlab forge.  
Dans le terminal de commande, vous pouvez switcher de l'ancien repo gitlab.com vers la nouvelle adresse gitlab-forge avec la commande `git remote set-url`
Par exemple pour le projet dreal-pdl/csd/utilitaires.ju, la commande : 
```
git remote set-url origin  https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/utilitaires.ju.git
```
va déclarer que le repo distant à suivre se trouve sur https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd. 


