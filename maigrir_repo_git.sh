# Etapes préliminaires : 
# --------------
# 1- Installer git filter-repo 
#     càd placer dans 'C:\Program Files (x86)\Git\mingw64\libexec\git-core' le fichier 'git-filter-repo' 
#     à telecharger depuis https://github.com/newren/git-filter-repo/blob/main/git-filter-repo
#     modifier l'entete de ce fichier avec le bloc note #!/usr/bin/env python3 en #!/usr/bin/env python si votre poste est windows
# 2- Ouvrir un terminal de commande au niveau du repertoire où se situe le dossier exporté (par exemple le dossier 'téléchargements')
#     Pour faire cela depuis RStudio : naviguer dans l'explorateur de fichier jusque au dossier 'téléchargements' 
#     Cliquer sur la roue crantée 'More' / Open new terminal here
# 3- Suivre le script ci-dessous

# Nommer ci-dessous le nom du projet exporté depuis gitlab (sans l'estension .tar.gz)
EXPORT=2022-09-28_08-58-133_dreal-datalab_si_eau_pesticide_export

 mkdir "$EXPORT"
 tar -xf "$EXPORT".tar.gz --directory="$EXPORT"/
 cd "$EXPORT"/
 git clone project.bundle

 # Prevent interference with recreating an importable file later
 mv project.bundle ../"$EXPORT"-original.bundle
 mv ../"$EXPORT".tar.gz ../"$EXPORT"-original.tar.gz

 
# Réduire la taille du fichier .git/objects/pack/ 
 cd project
# a utiliser pour ne créer qu'une seule branche à partir de l'ancien repo
 # git switch --create smaller-tmp-main
 # git reflog expire --expire=now --all
 # git gc --prune=now --aggressive
# retire le suivi historique des objets de plus 25 Mo et recompose l'historique
 git filter-repo --strip-blobs-bigger-than 25M --force

# Préparer un nouveau dossier tar.gz importable en listant ci dessous les branches à reimporter (master et suivantes)
 git bundle create ../project.bundle master dev mise-en-œuvre-version-v2_2-analyses-prelévements-pesticides-ESO # dev ou autre branche 
 cd ..
 mv project/ ../"$EXPORT"-project
 cd ..
 
  tar -czf "$EXPORT"-smaller.tar.gz --directory="$EXPORT"/ .
  